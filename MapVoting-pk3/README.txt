Created by Trillster to test new features of Zandronum 3.1
Props to Kaminsky for being super helpful with resolving some tickets required to make this work properly
Props to xHatahx for some of the CTF map previews
Props to Gh for the idea of opacity changing
Props to Thunderono for the idea of "!preview" command

If you're a mapper looking to see how map previews should be implemented, follow these steps:
Go into map in Deathmatch mode, set screenblocks 12, set crosshair 0, set r_drawspectatingstring false, and spectate.
Here's an alias for convenience
'alias screenie "spectate; crosshair 0; screenblocks 12; r_drawspectatingstring false"'
Screenshot map in 1024x768 resolution.
Downsize screenshot to 512x384 (50% of original size) with Nearest Neighbor filtering
Rename screenshot to be the same as your maps lumpname, in other words, MM2AIR's map preview is named MM2AIR.png
Toss screenshot into a textures folder in your map pack / addon.
This mod will recognize that graphic and implement it as a map preview

You can confirm that the texture is being recognized properly by loading your pack and Map Voting offline
and typing something like the following in chat: "!preview MM2AIR"